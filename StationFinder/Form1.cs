﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StationFinder
{
    public partial class Form1 : Form
    {
        // remember that two letter also is valid
        char[] LetterList = { ' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Æ', 'Ø', 'Å' };

        int a = -2, idx=1;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;                       
        }
       
        private void timer1_Tick(object sender, EventArgs e)
        {
            a++;

            if ((a % 63) == 0)
            {
                System.Diagnostics.Process.Start("firefox");
            }

            if ((a % 63) == 3)
            {                              
                for (int n = 0; n < LetterList.Length; n++)
                {
                    string s = "H" + LetterList[idx] + LetterList[n];
                    s = s.Trim();

                    System.Diagnostics.Process.Start("http://rtd.opm.jbv.no:8080/web_client/std?station=" + s + "&content=departure&layout=portrait&submit=G%C3%A5");
                }

                idx++;
            }
            
           if (idx == LetterList.Length)
            {
                timer1.Enabled = false;
            }

        }
    }
}
